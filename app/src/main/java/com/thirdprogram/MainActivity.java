package com.thirdprogram;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progress_bar);
        Button button = findViewById(R.id.download_button);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        List<String> list = new ArrayList<>();
        list.add("https://ssl-gfx.filmweb.pl/ph/08/62/862/182042.1.jpg");
        list.add("https://ssl-gfx.filmweb.pl/ph/10/48/1048/179578.1.jpg");
        list.add("https://ssl-gfx.filmweb.pl/ph/10/89/1089/212831.1.jpg");
        list.add("https://ssl-gfx.filmweb.pl/ph/02/08/208/183142_1.1.jpg");

        for (String items : list) {
            new GetImageAsyncTask(progressBar).execute(items);
        }

    }
}
